import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

//        ArrayList<String> fruits = new ArrayList<>(Arrays.asList("apple", "avocado", "banana", "kiwi", "orange"));
        String[] fruits;
        fruits = new String[]{"apple", "avocado", "banana", "kiwi", "orange"};
        System.out.println("Fruits in stock: " + Arrays.toString(fruits));

        Scanner userInput = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of?");
        String userChoice = userInput.nextLine();

//        int fruitIdx = fruits.indexOf(userChoice);
        int fruitIdx = Arrays.binarySearch(fruits, userChoice);
        System.out.println("The index of kiwi is: " + fruitIdx);

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Cloe", "Zoe"));
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 3);
        inventory.put("tootbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);


    }
}